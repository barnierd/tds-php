<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(
        "dureeExpiration" => 600
    );
    static public function getDureeExpiration(): ?int {
        return self::$configurationSite['dureeExpiration'];
    }
}