<body>
<form method="get">
    <fieldset>
        <?php
        if (isset($_COOKIE['preferenceControleur'])) {
            $prefControleur = $_COOKIE['preferenceControleur'];
            if ($prefControleur == "utilisateur") {
                $prefUtilisateur = "checked";
                $prefTrajet = "";
            } else if ($prefControleur == "trajet") {
                $prefTrajet = "checked";
                $prefUtilisateur = "";
            }
        }

        ?>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <input class="InputAddOn-field" <?= $prefUtilisateur ?? "" ?> type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur">
            <label class="InputAddOn-item" for="utilisateurId">Utilisateur</label>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" <?= $prefTrajet ?? "" ?> type="radio" id="trajetId" name="controleur_defaut" value="trajet">
            <label class="InputAddOn-item" for="trajetId">Trajet</label>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-item" type="submit" value="Envoyer" />
            <input class="InputAddOn-field" type='hidden' name='action' value='enregistrerPreference'>
        </p>
    </fieldset>
</form>
</body>