<body>
<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;


foreach ($trajets as $trajet) {
    echo '<a href ="controleurFrontal.php?action=afficherDetail&controleur=trajet&id=' . rawurlencode($trajet->getId()) . '">' . htmlspecialchars($trajet->getDepart() . " " . $trajet->getArrivee()) . " " . '</a>';
    echo '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=trajet&id='. rawurlencode($trajet->getId()).'"> Mettre à jour</a>';
    echo '<a href="controleurFrontal.php?action=supprimerTrajet&controleur=trajet&id='. rawurlencode($trajet->getId()).'"> Supprimer</a><br>';
}
echo '<a href ="controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet">Créer un trajet</a><br>';
?>
</body>
