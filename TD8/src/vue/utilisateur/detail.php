<body>
<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Modele\DataObject\Utilisateur;

$loginHTML = $utilisateur->getLogin();
$nomHTML = $utilisateur->getNom();
$prenomHTML = $utilisateur->getPrenom();

echo '<p> login :' . htmlspecialchars($loginHTML) . '.</p>';
echo '<p> nom :' . htmlspecialchars($nomHTML) . '.</p>';
echo '<p> prenom :' . htmlspecialchars($prenomHTML) . '.</p>';
?>
</body>
