<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    /*public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    public static function recupererPassagers(Trajet $trajet) : array {
        $tab = array();
        $requete = "SELECT u.* FROM utilisateur u INNER JOIN passager p ON u.login = p.passagerLogin WHERE p.trajetId = :trajetId";

        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $values = array(
            'trajetId' => $trajet->getId()
        );

        $pdo->execute($values);
        foreach ($pdo as $passager) {
            $tab[] = (new UtilisateurRepository())->construireDepuisTableauSQL($passager);
        }

        return $tab;
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {

        $trajettab = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau['conducteurLogin']), // À changer
            $trajetTableau["nonFumeur"],// À changer ?
            []
        );

        $trajettab->setPassagers(self::recupererPassagers($trajettab));
        return $trajettab;
    }

    protected function getNomTable(): string {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id","depart","arrivee","date","prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "id" => $trajet->getId(),
            "depart" => $trajet->getDepart(),
            "arrivee" => $trajet->getArrivee(),
            "date" => $trajet->getDate()->format("Y-m-d"),
            "prix" => $trajet->getPrix(),
            "conducteurLogin" => $trajet->getConducteur()->getLogin(),
            "nonFumeur" => $trajet->isNonFumeur() ? 1 : 0
        );
    }


    /*
    public function ajouter() : bool
    {
        $requete = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'depart' => $this->depart,
            'arrivee' => $this->arrivee,
            'date' => $this->date->format("Y/m/d"),
            'prix' => $this->prix,
            'conducteurLogin' => $this->conducteur->getLogin(),
            'nonFumeur' => $this->nonFumeur,
        ]);

        try {
            $this->id = ConnexionBaseDeDonnees::getPDO()->lastInsertId();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $requete = "DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'passagerLogin' => $passagerLogin,
            'trajetId' => $this->id
        ]);

        return $pdoStatement->rowCount() > 0;
    }

    public static function recupererTrajetParId(int $trajetId): ?Trajet {
        $requete = "SELECT * FROM trajet WHERE id = :trajetId";
        $pdo = ConnexionBaseDeDonnees::getPDO()->prepare($requete);
        $pdo->execute(['trajetId' => $trajetId]);

        $trajetTableau = $pdo->fetch(PDO::FETCH_ASSOC);

        if ($trajetTableau) {
            // Retourne un trajet avec des passagers vides pour le moment
            return (new TrajetRepository())->construireDepuisTableauSQL($trajetTableau);
        } else {
            return null;
        }
    }
    */
}