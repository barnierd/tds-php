<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateurs == null) {
            self::afficherVue('vueGenerale.php', ["titre" => "ERREUR", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateurs, "titre" => "Details de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs,"cheminCorpsVue" => "utilisateur/vueUtilisateurCree.php"]);
    }
}

?>
