<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
        // code donné
        /*$prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";*/

        // TEST CREATION TABLEAU
        /*$texte = [
                'prenom' => 'Tim',
                'nom' => 'Broussard'
        ];*/

        // TEST echo tableau
       // echo "$texte[nom], $texte[prenom]";

        // TEST AJOUT VALEUR DANS CLE
        // $texte['age'] = '19';

        // TEST AJOUT VALEUR SANS CLE
        // $texte[] = 'IUT Montpellier';

        //TEST FOREACH
        /*foreach ($texte as $cle => $valeur){
            echo "$cle : $valeur\n";
        }*/

        // EXERCICE 8

        $utilisateur = [
                'nom' => 'Carceles',
                'prenom' => 'Franck',
                'login' => 'carcelesf'
        ];

        echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]";

        $utilisateurs = [
                'tim','dodo','goatier'
            ];
            echo "\n <h1>Liste des utilisateurs :</h1> \n";
            foreach ($utilisateurs as $key => $value) {
                echo "<ul><li>", "$key : $value", "</li></ul> \n";
            }
            if (empty($utilisateurs)){
                echo "il n'y a aucun utilisateur.";
            }

        ?>
    </body>
</html> 