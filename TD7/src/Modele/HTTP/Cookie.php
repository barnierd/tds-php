<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        if ($dureeExpiration == null) {
            setcookie($cle, $valeur);
        } else {
            setcookie($cle, $valeur, time() + $dureeExpiration);
        }
    }

    public static function lire(string $cle): mixed {
        return $_COOKIE[$cle];
    }

    public static function contient(string $cle): bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer(string $cle): void {
        unset($_COOKIE[$cle]);
    }

}