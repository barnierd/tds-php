<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateurs = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateurs == null) {
            self::afficherErreur("Le login utilisé n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateurs, "titre" => "Details de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }


    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur crée !","login" => $utilisateur->getLogin(),'utilisateurs' => $utilisateurs,"cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function supprimerUtilisateur(): void{
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur supprimé !",'utilisateurs' => $utilisateurs, 'login' => $login,"cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "ERREUR","cheminCorpsVue" => "utilisateur/erreur.php","messageErreur" => $messageErreur]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Formulaire Mise à jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function mettreAJour(): void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs,'login' => $utilisateur->getLogin(),"titre" => "Utilisateur mis à jour !", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
    }
}

