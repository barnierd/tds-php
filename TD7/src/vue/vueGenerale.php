<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../ressources/navstyle.css">
<head>
    <meta charset="UTF-8">
    <title> <?php
        /**
         * @var string $titre
         */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/heart.png" alt=""></a>
            </li>
        </ul>
    </nav>

</header>
<main>
    <?php
     /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>Site de covoiturage de la ZICCORP</p>
</footer>
</body>
</html>

