<body>
<form method="get">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <?php

        use App\Covoiturage\Modele\DataObject\Utilisateur;

        /**
         * @var Utilisateur $utilisateur
         */
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $nomHTML = htmlspecialchars($utilisateur->getNom());
        $prenomHTML = htmlspecialchars($utilisateur->getPrenom());
        ?>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input value ="<?= $loginHTML ?> " class="InputAddOn-field" type="text" placeholder="broussardt" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="name_id">Nom</label>
            <input value="<?= $nomHTML ?>" class="InputAddOn-field" type="text" placeholder="Broussard" name="nom" id="name_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="surname_id">Prenom</label>
            <input readonly value="<?= $prenomHTML ?>" class="InputAddOn-field" type="text" placeholder="Tim" name="prenom" id="surname_id" required/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-item" type="submit" value="Envoyer" />
            <input class="InputAddOn-field" type='hidden' name='controleur' value='utilisateur'>
            <input class="InputAddOn-field" type='hidden' name='action' value='mettreAJour'>
        </p>
    </fieldset>
</form>
</body>
