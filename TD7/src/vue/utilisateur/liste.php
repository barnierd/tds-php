<body>
<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;


foreach ($utilisateurs as $utilisateur) {
    echo '<a href ="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=' . rawurlencode($utilisateur->getLogin()) . '">' . htmlspecialchars($utilisateur->getLogin()) . '</a>';
    echo '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login='. rawurlencode($utilisateur->getLogin()).'"> Mettre à jour</a>';
    echo '<a href="controleurFrontal.php?action=supprimerUtilisateur&controleur=utilisateur&login='. rawurlencode($utilisateur->getLogin()).'"> Supprimer</a><br>';
}
    echo '<a href ="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">Créer un utilisateur</a><br>';
?>
</body>
