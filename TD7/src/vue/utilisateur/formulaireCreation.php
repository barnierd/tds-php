<body>
<form method="get">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="broussardt" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="name_id">Nom</label>
            <input class="InputAddOn-field" type="text" placeholder="Broussard" name="nom" id="name_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="surname_id">Prenom</label>
            <input class="InputAddOn-field" type="text" placeholder="Tim" name="prenom" id="surname_id" required/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-item" type="submit" value="Envoyer" />
            <input class="InputAddOn-field" type='hidden' name='controleur' value='utilisateur'>
            <input class="InputAddOn-field" type='hidden' name='action' value='creerDepuisFormulaire'>
        </p>
    </fieldset>
</form>
</body>
