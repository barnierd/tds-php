<body>
<?php
/** @var Trajet $trajet */

use App\Covoiturage\Modele\DataObject\Trajet;

$nonFumeur = !$trajet->isNonFumeur() ? " non fumeur" : "";

echo "<p>
        Le trajet{$nonFumeur} du {$trajet->getDate()->format("d/m/Y")} partira de {$trajet->getDepart()} pour aller à {$trajet->getArrivee()} (conducteur: {$trajet->getConducteur()->getPrenom()} {$trajet->getConducteur()->getNom()}).<br/>
    </p><br>    ";
?>
</body>
