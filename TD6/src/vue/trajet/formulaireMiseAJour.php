<body>
<form method="get">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <?php

        use App\Covoiturage\Modele\DataObject\Trajet;

        /**
         * @var Trajet $trajet
         */

        $idHTML = htmlspecialchars($trajet->getId());
        $departHTML = htmlspecialchars($trajet->getDepart());
        $arriveeHTML = htmlspecialchars($trajet->getArrivee());
        $dateHTML = $trajet->getDate()->format("Y-m-d");
        $prix = $trajet->getPrix();
        $conducteurLoginHTML = $trajet->getConducteur()->getLogin();
        if ($trajet->isNonFumeur()) {
            $nonFumeur = "checked";
        } else {
            $nonFumeur = "";
        }

        ?>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart</label>
            <input class="InputAddOn-field" value="<?= $departHTML ?>" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée</label>
            <input class="InputAddOn-field" value="<?= $arriveeHTML ?>" type="text" placeholder="Nîmes" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date</label>
            <input class="InputAddOn-field" value="<?= $dateHTML ?>" type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id" />
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix</label>
            <input class="InputAddOn-field" value="<?= $prix ?>" type="text" placeholder="100" name="prix" id="prix_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur</label>
            <input class="InputAddOn-field" value="<?= $conducteurLoginHTML ?>" type="text" placeholder="barnierd" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur ?</label>
            <input class="InputAddOn-field" <?= $nonFumeur ?> type="checkbox" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-item" type="submit" value="Envoyer" />
            <input class="InputAddOn-field" type='hidden' name='controleur' value='trajet'>
            <input class="InputAddOn-field" type='hidden' name='id' value='<?= $idHTML ?>'>
            <input class="InputAddOn-field" type='hidden' name='action' value='mettreAJour'>
        </p>
    </fieldset>
</form>
</body>
