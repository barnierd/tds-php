<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
    }

    protected function getNomTable(): string {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return ["login","nom","prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom(),
        );
    }


}