<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use Exception;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $tabColonnes = $this->getNomsColonnes();
        for ($i = 0; $i < count($tabColonnes); $i++) {
            $tabColonnes[$i] = $tabColonnes[$i] . "= :" . $tabColonnes[$i];
        }
        $sql = "UPDATE ". $this->getNomTable() ." SET ". join(", ",  $tabColonnes) ." WHERE ". $this->getNomClePrimaire() ." = :". $this->getNomClePrimaire();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $sql = "INSERT INTO ". $this->getNomTable() . "(" . join(",",$this->getNomsColonnes()) . ")" ." VALUES (". ":" . join(", :", $this->getNomsColonnes()).")";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
        try {
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM ". $this->getNomTable() ." WHERE ". $this->getNomClePrimaire() ." = :idTable";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTable" => $valeurClePrimaire,
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $idTable): ?AbstractDataObject
    {
        $sql = "SELECT * from ". $this->getNomTable() ." WHERE " . $this->getNomClePrimaire() ." = :idTable";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTable" => $idTable,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable() . ";");

        foreach ($pdoStatement as $objetFormatTableau) {
            $tableauUtilisateurs[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $tableauUtilisateurs;
    }

    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;


}