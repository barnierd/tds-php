<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "ERREUR","cheminCorpsVue" => "trajet/erreur.php","messageErreur" => $messageErreur]);
    }

    public static function afficherDetail(): void
    {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Le login utilisé n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "Details de l'utilisateur", "cheminCorpsVue" => "trajet/detail.php"]);
        }
    }

    public static function supprimerTrajet(): void{
        $id = $_GET['id'];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur supprimé !",'trajets' => $trajets, 'id' => $id,"cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }

    public static function creerTrajet(): void
    {
        $trajet = self::creerDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Trajet crée !","id" => $trajet->getId(),'trajets' => $trajets,"cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    public static function mettreAJour(): void {
        $trajet = self::creerDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets,'id' => $trajet->getId(),'login' => $trajet->getConducteur()->getLogin(),"titre" => "Trajet mis à jour !", "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        self::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "Formulaire Mise à jour", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire Utilisateur", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    /**
     * @return Trajet
     * @throws \Exception
     */
    public static function creerDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $date = new DateTime($tableauDonneesFormulaire['date']);
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']);
        $trajet = new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], $date, $tableauDonneesFormulaire['prix'], $utilisateur, isset($tableauDonneesFormulaire['nonFumeur']), []);
        return $trajet;
    }


}