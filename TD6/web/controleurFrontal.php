<?php

require_once __DIR__ . '/../src/lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurUtilisateur;


if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "afficherListe";
}

if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
} else {

    $controleur = "utilisateur";
}

$nomDeClasseControleur = ("App\Covoiturage\Controleur\Controleur" . ucfirst($controleur));
if (class_exists($nomDeClasseControleur)) {
    $methods = get_class_methods($nomDeClasseControleur);
    if (in_array($action, $methods)) {
        $nomDeClasseControleur::$action();
    } else {
        ControleurUtilisateur::afficherErreur("Cette action n'existe pas : $action");
    }
} else {
    ControleurUtilisateur::afficherErreur("Ce controleur n'existe pas : $controleur");
}

