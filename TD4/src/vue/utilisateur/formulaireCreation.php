<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>formulaireUtilisateur</title>
</head>
<body>
<form method="get">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="login_id">Login</label> :
            <input type="text" placeholder="broussardt" name="login" id="login_id" required/>
        </p>
        <p>
            <label for="name_id">Nom</label> :
            <input type="text" placeholder="Broussard" name="nom" id="name_id" required/>
        </p>
        <p>
            <label for="surname_id">Prenom</label>
            <input type="text" placeholder="Tim" name="prenom" id="surname_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
        </p>
    </fieldset>
</form>
</body>
</html>

