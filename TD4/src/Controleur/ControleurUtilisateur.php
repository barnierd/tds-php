<?php

require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateurs == null) {
            self::afficherVue('utilisateur/erreur.php');
        } else {
            self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateurs]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        self::afficherListe();
    }
}

?>
